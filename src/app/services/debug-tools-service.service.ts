import {Injectable} from '@angular/core';
import {ServerService} from './server.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { LocalStorage } from 'ngx-store';

@Injectable({
  providedIn: 'root'
})
export class DebugToolsServiceService {

  @LocalStorage() debugState = false;

  constructor(private serverService: ServerService,
              private http: HttpClient) {
  }

  // CORS header
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    })
  };

  pingServer() {
    return this.http.get(this.serverService.serveraddress + 'debug', this.httpOptions);
  }

  switchToLocalServer() {
    this.serverService.serveraddress = 'http://localhost:8080/';
  }

  switchToProdServer() {
    this.serverService.serveraddress = 'http://readdeo.dynu.net:8080/';
  }

  printDebugState() {
    console.log(this.debugState);
  }

  printCurrentServerAddress() {
    console.log(this.serverService.serveraddress);
  }
}
