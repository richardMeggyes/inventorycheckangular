import { TestBed } from '@angular/core/testing';

import { DebugToolsServiceService } from './debug-tools-service.service';

describe('DebugToolsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DebugToolsServiceService = TestBed.get(DebugToolsServiceService);
    expect(service).toBeTruthy();
  });
});
