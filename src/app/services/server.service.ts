import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NotificationService} from '../shared/services/notification.service';
import { LocalStorage } from 'ngx-store';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  @LocalStorage() serveraddress = 'http://readdeo.dynu.net:8080/';


  constructor(private http: HttpClient,
              private notificationService: NotificationService) {
  }

  // CORS header
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    })
  };

  deleteUser() {
    return this.http.delete(this.serveraddress + 'user-settings');
  }

  createNewJob(jobConfiguration) {
    console.log('jobConfiguration');
    console.log(jobConfiguration.toString());
    console.log(jobConfiguration);
    const uploadData = new FormData();
    uploadData.append('newJobConfiguration', JSON.stringify(jobConfiguration));
    this.http.put(this.serveraddress + 'job', uploadData, this.httpOptions)
      .subscribe(event => {
          console.log('Upload done' + event['status']);
          if (event['status'] === 'success') {
            this.notificationService.showSuccess('', 'Successfully created new job!');
          } else if (event['status'].contains('Error:')) {
            this.notificationService.showError('', event['status']);
          }
        },
        error => this.handleHttpError(error));
  }

  getJobs() {
    return this.http.get(this.serveraddress + 'job');
  }

  createArrayFromJSONArray(data) {
    const jobs = new Array<JSON>();
    let counter = 0;
    while (data[counter] !== undefined) {
      const job = data[counter];
      console.log(job);
      jobs.push(job);
      counter++;
    }
    console.log('jobsjson? ' + jobs);
    return jobs;
  }

  handleHttpError(error) {
    if (error.name === 'HttpErrorResponse') {
      this.notificationService.showError(error.message, 'Failed to reach server!');
    } else {
      this.notificationService.showError(error.message, error.name);
    }
  }

  addnewDevice(name, user, deviceholdersname) {
    const uploadData = new FormData();
    uploadData.append('name', name);
    uploadData.append('user', user);
    uploadData.append('deviceholdersname', deviceholdersname);
    return this.http.put(this.serveraddress + 'device', uploadData, this.httpOptions)
      .subscribe(event => {
          if (event['status'] === 'success') {
            this.notificationService.showSuccess('', 'Successfully added new device!');
            return true;
          } else if (event['status'].contains('Error:')) {
            this.notificationService.showError('', event['status']);
            return false;
          }
        },
        error => this.handleHttpError(error));
  }

  getDevices() {
    return this.http.get(this.serveraddress + 'device', this.httpOptions);
  }

  getProjects() {
    const endPoint = 'converter-projects';
    return this.http.get(this.serveraddress + endPoint, this.httpOptions);
  }

  switchProjectActive(projectId) {
    const uploadData = new FormData();
    uploadData.append('projectId', projectId);
    return this.http.post(this.serveraddress + 'converter-projects', uploadData, this.httpOptions);
  }

  removeDevices() {
    return this.http.delete(this.serveraddress + 'converter-progress', this.httpOptions);
  }

  reloadFiles() {
    return this.http.get(this.serveraddress + 'readfiles', this.httpOptions);
  }
}
