import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ServerService} from './server.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = this.serverService.serveraddress + 'api/test/user';
  private pmUrl = this.serverService.serveraddress + 'api/test/pm';
  private adminUrl = this.serverService.serveraddress + 'api/test/admin';

  constructor(private http: HttpClient, private serverService: ServerService) { }

  getUserBoard(): Observable<string> {
    return this.http.get(this.userUrl, { responseType: 'text' });
  }

  getPMBoard(): Observable<string> {
    return this.http.get(this.pmUrl, { responseType: 'text' });
  }

  getAdminBoard(): Observable<string> {
    return this.http.get(this.adminUrl, { responseType: 'text' });
  }
}
