import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SidebarComponent} from './layouts/components/sidebar/sidebar.component';
import {NavbarComponent} from './layouts/components/navbar/navbar.component';
import {FooterComponent} from './layouts/components/footer/footer.component';
import {AppRoutingModule} from './app-routing.module';
import {MainLayoutComponent} from './layouts/main-layout/main-layout.component';
import {FormsModule} from '@angular/forms';

import {JobsComponent} from './main-components/jobs/jobs.component';
import {NewJobComponent} from './main-components/jobs/new-job/new-job.component';
import {DevicesComponent} from './main-components/devices/devices.component';
import {SettingsComponent} from './main-components/settings/settings.component';
import {DashboardComponent} from './main-components/dashboard/dashboard.component';
import {AddDataColumnsComponent} from './main-components/jobs/new-job/add-data-columns/add-data-columns.component';
import {HttpClientModule} from '@angular/common/http';
import {TableUploadComponent} from './main-components/jobs/new-job/table-upload/table-upload.component';
import {LoginComponent} from './auth/login/login.component';
import {AdminComponent} from './main-components/admin/admin.component';
import {HomeComponent} from './main-components/home/home.component';
import {RegisterComponent} from './auth/register/register.component';
import {UserComponent} from './user/user.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {AuthGuardService as AuthGuard} from './auth/auth-guard.service';
import {RoleGuardService as RoleGuard} from './auth/role-guard.service';
import {NopermissionComponent} from './auth/nopermission/nopermission.component';
import {CategoryBuilderComponent} from './main-components/jobs/new-job/category-builder/category-builder.component';
import {ColumnTypeProvider} from './providers/ColumnTypes';
import {ExamplePlaceHolderProvider} from './providers/ExamplePlaceHolders';
import {DataTypeProvider} from './providers/DataType';
import {ServerService} from './services/server.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {NotificationService} from './shared/services/notification.service';
import { ConvertLogComponent } from './main-components/convert-log/convert-log.component';
import { DebugToolsComponent } from './main-components/debug-tools/debug-tools.component';

import { WebStorageModule } from 'ngx-store';
import {DebugToolsServiceService} from './services/debug-tools-service.service';
import {MagazineIndexerModule} from './magazine-indexer/magazine-indexer.module';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    MainLayoutComponent,

    // Jobs
    JobsComponent,
    NewJobComponent,
    TableUploadComponent,
    CategoryBuilderComponent,

    DevicesComponent,
    SettingsComponent,
    DashboardComponent,
    AddDataColumnsComponent,

    // AUTH
    LoginComponent,
    UserComponent,
    RegisterComponent,
    HomeComponent,
    AdminComponent,
    NopermissionComponent,
    ConvertLogComponent,
    DebugToolsComponent
  ],
  imports: [
    WebStorageModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MagazineIndexerModule
  ],
  exports: [CategoryBuilderComponent],
  providers: [httpInterceptorProviders, AuthGuard, RoleGuard, ColumnTypeProvider,
    ExamplePlaceHolderProvider, DataTypeProvider, ServerService,
    NotificationService, DebugToolsServiceService],
  bootstrap: [AppComponent],
  entryComponents: [CategoryBuilderComponent]
})
export class AppModule {
}
