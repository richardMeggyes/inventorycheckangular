export class ColumnTypeProvider {
  public CCID = 'CCID';
  public SWITCH = 'SWITCH';
  public NUMBER = 'NUMBER';
  public SELECTOR = 'SELECTOR';
  public TEXT = 'TEXT';
  public PHOTO = 'PHOTO';
  public CATEGORY_SELECTOR = 'CATEGORY';
}
