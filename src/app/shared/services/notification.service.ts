import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor() {
  }

  showSuccess(message = '', title = 'Success!') {
    // this.toastr.successToastr(message, title);
  }

  showError(message = '', title = 'ERROR!') {
    // this.toastr.errorToastr(message, title);
  }

  showWarning(message = '', title = 'Warning!') {
    // this.toastr.warningToastr(message, title);
  }

  showInfo(message = '', title = 'Info') {
    // this.toastr.infoToastr(message, title);
  }

  showCustom(message = 'custom', title = '') {
    // this.toastr.customToastr(
    //   '<span style=\'color: green; font-size: 16px; text-align: center;\'><i class="material-icons">email</i>Custom Toast</span>',
    //   null,
    //   {enableHTML: true}
    // );
  }

  showToast(position: any = 'top-left', message, title = '') {
    // this.toastr.infoToastr(message, title, {
    //   position: position
    // });
  }

  toastDemo() {
    // this.showSuccess('Message', 'Title');
    // this.showError('Message', 'Title');
    // this.showInfo('Message', 'Title');
    // this.showCustom('Message', 'Title');
    // this.showToast('Message', 'Title');
  }
}
