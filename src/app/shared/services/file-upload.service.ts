import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private httpClient: HttpClient) {
  }

  postFile(fileToUpload: File) {
    const endpoint = 'http://localhost:8080/upload';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    this.httpClient.post(endpoint, formData).pipe(map(data => {
    })).subscribe(result => {
      console.log(result);
    });
  }
}
