import {TableColumn} from './table-column';

export class CategoryColumn extends TableColumn {
  categoryId: string;

  constructor(name, type, categoryId) {
    super(name, type);
    this.categoryId = categoryId;
  }
}
