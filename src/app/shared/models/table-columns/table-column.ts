export class TableColumn {
  id: number;
  name: string;
  type: string;

  constructor(name, type) {
    this.name = name;
    this.type = type;
  }

  toString() {
    return 'class TableColumn: id:' + this.id +
      ' name: ' + this.name +
      ' type: ' + this.type;
  }
}
