import {TableColumn} from './table-column';

export class SelectorColumn extends TableColumn {
  selectorValues: string;

  constructor(name, type, selectorValues = '') {
    super(name, type);
    this.selectorValues = selectorValues;
  }
}
