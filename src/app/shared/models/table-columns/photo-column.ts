import {TableColumn} from './table-column';

export class PhotoColumn extends TableColumn {
  switchBeforeTakePhotoEnabled = false;

  constructor(name, type) {
    super(name, type);
  }
}
