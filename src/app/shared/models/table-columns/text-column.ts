import {TableColumn} from './table-column';

export class TextColumn extends TableColumn {
  autoDropDownEnabled = false;

  constructor(name, type) {
    super(name, type);
  }
}
