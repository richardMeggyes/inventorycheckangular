export class SubCategory {
  name: string;
  highlight = false;
  error = false;
  color: string;

  constructor(name) {
    this.name = name;
  }

  toString() {
    return 'class CategoryModel:' +
      '\r\nname: ' + this.name +
      '\r\nhighlight: ' + this.highlight;
  }
}
