export class Tablefile {
  header: Array<string>;
  localFileName: string;
  dataType: string; // reference, costCenter
  ccColumn: number;
  firstRowIsData: boolean;
  fileId: number;

  constructor() {
    this.firstRowIsData = false;
  }

  toString() {
    return 'header: ' + this.header +
      '\r\n localFileName: ' + this.localFileName +
      '\r\n dataType: ' + this.dataType +
      '\r\n ccColumn: ' + this.ccColumn +
      '\r\n firstRowIsData: ' + this.firstRowIsData +
      '\r\b file id: ' + this.fileId;
  }
}
