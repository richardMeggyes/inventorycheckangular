import {SubCategory} from './SubCategory';

export class CategoryModel {
  name: string;
  subcategories: Array<SubCategory>;
  highlight = false;
  error = false;
  color: string;

  constructor(name, subcategories) {
    this.name = name;
    this.subcategories = subcategories;
  }

  toString() {
    return 'class CategoryModel:' +
      '\r\nname: ' + this.name +
      '\r\nsubcategories: ' + this.subcategories;
  }
}
