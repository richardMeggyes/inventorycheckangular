import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagazineMainComponent } from './magazine-main.component';

describe('MagazineMainComponent', () => {
  let component: MagazineMainComponent;
  let fixture: ComponentFixture<MagazineMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagazineMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagazineMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
