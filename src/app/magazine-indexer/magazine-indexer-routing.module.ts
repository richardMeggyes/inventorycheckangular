import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MagazineMainComponent} from './magazine-main/magazine-main.component';

const routes: Routes = [
  { path: 'magazine', component: MagazineMainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagazineIndexerRoutingModule { }
