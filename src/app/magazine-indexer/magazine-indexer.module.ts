import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MagazineIndexerRoutingModule } from './magazine-indexer-routing.module';
import { MagazineMainComponent } from './magazine-main/magazine-main.component';

@NgModule({
  declarations: [MagazineMainComponent],
  imports: [
    CommonModule,
    MagazineIndexerRoutingModule
  ]
})
export class MagazineIndexerModule { }
