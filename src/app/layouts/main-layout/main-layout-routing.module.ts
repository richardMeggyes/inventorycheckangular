import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes} from '@angular/router';

const routes: Routes = [
  // {path: 'jobs', component: JobsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class MainLayoutRoutingModule { }
