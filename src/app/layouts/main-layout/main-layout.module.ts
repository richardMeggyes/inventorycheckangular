import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MainLayoutComponent } from './main-layout.component';
// import {JobsModule} from '../../main-components/jobs/jobs.module';
import {MainLayoutRoutingModule} from './main-layout-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  // declarations: [MainLayoutComponent],
  imports: [
    // JobsModule,
    CommonModule,
    MainLayoutRoutingModule,
    FormsModule
  ],
  providers: []
})
export class MainLayoutModule { }
