import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {interval} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';
import {TokenStorageService} from '../../auth/token-storage.service';
import {NotificationService} from '../../shared/services/notification.service';
import {ServerService} from '../../services/server.service';

@Component({
  selector: 'app-convert-log',
  templateUrl: './convert-log.component.html',
  styleUrls: ['./convert-log.component.css']
})
export class ConvertLogComponent implements OnInit {

  data: any;

  progress = 10;
  progressStr = '0%';
  totalProgress = 0;
  totalProgressString = '0.0%';

  totalRemaining = '';
  totalElapsed = '';

  progressBarStyle = 'left: 0%; right: 10%;';

  // Files
  clientsStatus: any;
  videoFiles = new Array<JSON>();
  videoFilesWaiting = new Array<JSON>();
  videoFilesInProgress = new Array<JSON>();
  videoFilesFinished = new Array<JSON>();
  videoFilesError = new Array<JSON>();
  totalFiles = 0;
  statuses = {};
  subdirs: string[];
  dirNames = '{"" : ""}';
  dirStat: any;

  // Projects
  projects = [];
  projectsFinished = [];

  constructor(private http: HttpClient,
              private sanitizer: DomSanitizer,
              private tokenStorageService: TokenStorageService,
              private notificationService: NotificationService,
              private serverService: ServerService) {
  }

  // CORS header
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    })
  };

  ngOnInit() {
    this.getFilesToConvert();
    this.getProjects();
    this.getStatusUpdate();
    interval(5000).subscribe(x => {
      // this.getProgress();
      // console.log('' + this.totalProgress);
      this.getStatusUpdate();
    });
  }

  countFinishedPerProject() {
    for (const project of this.projects) {
      for (const videoFile of this.videoFiles) {
        if (videoFile['path'] === project['name'] && videoFile['status'] === 2) {
          if (this.projectsFinished[project['name']] === undefined) {
            this.projectsFinished[project['name']] = 0;
          }
          this.projectsFinished[project['name']] = this.projectsFinished[project['name']] + 1;
        }
      }
    }
  }

  calculateStatuses() {
    const statuses = {};

    for (const videoFile of this.videoFiles) {

      if (statuses[videoFile['status']] === undefined) {
        statuses[videoFile['status']] = 1;
      } else {
        statuses[videoFile['status']] += 1;
      }
    }
    this.totalFiles = this.getTotalVideoFiles();
    this.statuses = statuses;
  }

  getTotalVideoFiles() {
    let videoFiles = 0;
    videoFiles += this.videoFilesWaiting.length;
    videoFiles += this.videoFilesInProgress.length;
    videoFiles += this.videoFilesFinished.length;
    videoFiles += this.videoFilesError.length;
    return videoFiles;
  }

  getFilesToConvert() {
    console.log('getFilesToConvert');
    this.http.get(this.serverService.serveraddress + 'converter', this.httpOptions).subscribe(data => {
      console.log('getFilesToConvert request');
      const videoFiles = <JSON[]>JSON.parse(data['videoFiles']);

      this.sortVideoFiles(videoFiles);
      this.calculateStatuses();

      this.subdirs = data['subdirs'].split(',');

      this.dirNames = data['dirNames'];
      this.dirStat = JSON.parse(data['dirStat']);
      console.log(videoFiles);

      console.log(this.dirNames);
      const totalProgress = +((+this.statuses[2]) / videoFiles.length * 100).toString();
      this.totalProgress = totalProgress;
      this.buildTotalProgressString(totalProgress);
      this.countFinishedPerProject();
    });
  }

  changeStatus(videoFile) {
    const authorities = this.tokenStorageService.getAuthorities();
    console.log(authorities);
    let hasAdminRole = false;
    for (const role of authorities) {
      if (role === 'ROLE_ADMIN') {
        hasAdminRole = true;
      }
    }

    if (hasAdminRole) {

      const uploadData = new FormData();
      uploadData.append('videoFileId', videoFile.id);
      this.http.put(this.serverService.serveraddress + 'converter-progress', uploadData, this.httpOptions).subscribe(data => {
        if (data['status'] === 'success') {
          videoFile.status = data['newStatus'];
        }
        this.calculateStatuses();
      });
    } else {
      this.notificationService.showError('You do not have permission to modify the statuses');
    }
  }

  getStatusUpdate() {
    this.http.get(this.serverService.serveraddress + 'converter-progress', this.httpOptions).subscribe(data => {
      const timeNow = new Date().getTime();
      for (const converter of <Array<JSON>>data) {
        // Adding elapsed time value to the converter's status
        const elapsedTime = timeNow - converter['lastSeen'];
        converter['elapsedTime'] = elapsedTime;

        // Adding progress % to the converter's status
        const progressPercent = +converter['frame'] / +converter['totalFrames'] * 100;
        converter['progressPercent'] = progressPercent.toFixed(2);
      }
      this.clientsStatus = data;
      console.log(this.clientsStatus);
    });
  }

  sortVideoFiles(videoFiles) {
    let loopCounter = 0;
    while (loopCounter < 4) {
      console.log(loopCounter);
      for (const videoFile of videoFiles) {
        if (loopCounter === 0 && videoFile['status'] === 1) {
          this.videoFiles.push(videoFile);
        } else if (loopCounter === 1 && videoFile['status'] === 0) {
          this.videoFiles.push(videoFile);
        } else if (loopCounter === 2 && videoFile['status'] === 2) {
          this.videoFiles.push(videoFile);
        } else if (loopCounter === 3 && videoFile['status'] === 3) {
          this.videoFiles.push(videoFile);
        }
      }
      loopCounter++;
    }
  }

  switchClientStatus(clientId) {
    const uploadData = new FormData();
    uploadData.append('clientId', clientId);
    uploadData.append('operation', 'switchActive');
    this.http.put(this.serverService.serveraddress + 'converter-client', uploadData, this.httpOptions).subscribe(data => {
      const newStatus = data['newStatus'];
    });
  }

  buildTotalProgressString(progress: number) {
    this.totalProgressString = progress.toPrecision(2).toString();
  }

  getProjects() {
    this.serverService.getProjects().subscribe(data => {
      console.log('getProjects');
      console.log(data);
      for (const project of data['projects']) {
        console.log(project);
        this.projects.push(project);
      }
      console.log('getProjects end');
    });
  }

  switchProjectActive(project) {
    const prevVal = project['active'];
    this.serverService.switchProjectActive(project['id']).subscribe(data => {
      project['active'] = data['projectStatus'];
      console.log(project['active']);
      console.log(prevVal);
      if (prevVal === project['active']) {
        this.notificationService.showError('Failed to switch project active status');
      }
    });
  }

  removeDevices() {
    this.serverService.removeDevices().subscribe(data => {
    });
    this.getStatusUpdate();
  }

  reloadfiles() {
    this.serverService.reloadFiles().subscribe(data => {
      console.log('Files reloaded. Results:');
      console.log(data);
    });
  }
}
