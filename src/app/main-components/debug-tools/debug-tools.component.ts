import {Component, OnInit} from '@angular/core';
import {DebugToolsServiceService} from '../../services/debug-tools-service.service';
import {ServerService} from '../../services/server.service';
import {interval} from 'rxjs';

@Component({
  selector: 'app-debug-tools',
  templateUrl: './debug-tools.component.html',
  styleUrls: ['./debug-tools.component.css']
})
export class DebugToolsComponent implements OnInit {

  serverLastSeen = 0;
  serverLastSeenElapsed = 99999;

  constructor(private debugToolsService: DebugToolsServiceService,
              private serverService: ServerService) {
  }

  ngOnInit() {

    interval(250).subscribe(x => {
      this.pingServer();
      this.serverLastSeenElapsed = new Date().getTime() - this.serverLastSeen;
    });

  }

  changeDebugState() {
    this.debugToolsService.debugState = !this.debugToolsService.debugState;
  }

  pingServer() {
    this.debugToolsService.pingServer().subscribe( data => {
      if (data['status'] === 'success') {
        this.serverLastSeen = new Date().getTime();
      }
    });
  }

  changeToLocalServer() {
    this.debugToolsService.switchToLocalServer();
  }

  changeToProdServer() {
    this.debugToolsService.switchToProdServer();
  }

  printADebugState() {
    this.debugToolsService.printDebugState();
  }

  printACurrentServerAddress() {
    this.debugToolsService.printCurrentServerAddress();
  }

}
