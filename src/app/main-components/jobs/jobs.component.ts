import {Component, OnInit} from '@angular/core';
import {ServerService} from '../../services/server.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  inProgressTab = true;
  doneTab = false;

  jobs =  Array<JSON>();

  constructor(private serverService: ServerService) {
  }

  ngOnInit() {
    this.serverService.getJobs().subscribe(data => (
        this.populateJobs(data)
      )
    );
    console.log('JOBS: ' + this.jobs);
  }

  tabSelect(tabSelect) {
    if (tabSelect === 'inprogress') {
      this.inProgressTab = true;
      this.doneTab = false;
    } else if (tabSelect === 'done') {
      this.inProgressTab = false;
      this.doneTab = true;
    }
  }

  populateJobs(jobsJSON) {
    console.log(jobsJSON);
    let counter = 0;
    while (jobsJSON[counter] !== undefined) {
      const job = jobsJSON[counter];
      console.log('JOB: ' + job);
      this.jobs.push(job);
      counter++;
    }
  }
}
