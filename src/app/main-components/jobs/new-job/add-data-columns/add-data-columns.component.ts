import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TableColumn} from '../../../../shared/models/table-columns/table-column';
import {ColumnTypeProvider} from '../../../../providers/ColumnTypes';
import {TextColumn} from '../../../../shared/models/table-columns/text-column';
import {PhotoColumn} from '../../../../shared/models/table-columns/photo-column';
import {CategoryColumn} from '../../../../shared/models/table-columns/category-column';
import {SelectorColumn} from '../../../../shared/models/table-columns/selector-column';

@Component({
  selector: 'app-add-data-columns',
  templateUrl: './add-data-columns.component.html',
  styleUrls: ['./add-data-columns.component.css']
})
export class AddDataColumnsComponent implements OnInit {

  @Input()
  tableColumns: Array<any>;

  @Output()
  categoryColumnAdded = new EventEmitter<string>();

  @Output()
  removeCategoryColumn = new EventEmitter<string>();

  columnType: ColumnTypeProvider;

  constructor(columnType: ColumnTypeProvider) {
    this.columnType = columnType;
  }

  ngOnInit() {
  }

  addNewTableColumn(type) {
    if (type === this.columnType.CCID) {
      this.tableColumns.push(new TableColumn('', this.columnType.CCID));
    } else if (type === this.columnType.NUMBER) {
      this.tableColumns.push(new TableColumn('', this.columnType.NUMBER));
    } else if (type === this.columnType.SWITCH) {
      this.tableColumns.push(new TableColumn('', this.columnType.SWITCH));
    } else if (type === this.columnType.SELECTOR) {
      this.tableColumns.push(new SelectorColumn('', this.columnType.SELECTOR));
    } else if (type === this.columnType.TEXT) {
      this.tableColumns.push(new TextColumn('', this.columnType.TEXT));
    } else if (type === this.columnType.PHOTO) {
      this.tableColumns.push(new PhotoColumn('', this.columnType.PHOTO));
    } else if (type === this.columnType.CATEGORY_SELECTOR) {
      const categoryId = this.getNextCategoryId();
      const categoryTableColumn = new CategoryColumn('', this.columnType.CATEGORY_SELECTOR, categoryId);
      this.tableColumns.push(categoryTableColumn);
      this.categoryColumnAdded.emit(categoryId);
    } else {
      console.log('Type not found while adding new TableColumn');
    }
  }

  getNextCategoryId() {
    if (this.tableColumns.length === 0) {
      console.log('CATEGORY_1');
      return 'CATEGORY_1';
    }

    const idContainer = Array<number>();

    for (const tableColumn of this.tableColumns) {
      if (tableColumn instanceof CategoryColumn) {
        const categoryColumnId = +tableColumn.categoryId.split('_')[1];
        idContainer.push(categoryColumnId);
      }
    }
    let counter = 1;
    while (true) {
      if (idContainer.indexOf(counter) === -1) {
        console.log('CATEGORY_' + counter + ' idcontainer: ' + idContainer);
        return 'CATEGORY_' + counter;
      }
      if (counter > 100000) {
        console.log('Error, next cat id too high ' + idContainer);
        break;
      }
      counter++;
    }
  }

  modifyColumnType(id, type) {
    this.tableColumns[id].type = type;
  }

  removeColumn(index) {
    const tableColumn = this.tableColumns[index];
    if (tableColumn instanceof CategoryColumn) {
      this.removeCategoryColumn.emit(tableColumn.categoryId);
    }
    this.tableColumns.splice(index, 1);
  }

  columnSpecialAttributeSwitch(id) {
    if (this.tableColumns[id] instanceof TextColumn) {
      this.tableColumns[id].autoDropDownEnabled = !this.tableColumns[id].autoDropDownEnabled;
    } else if (this.tableColumns[id] instanceof PhotoColumn) {
      this.tableColumns[id].switchBeforeTakePhotoEnabled = !this.tableColumns[id].switchBeforeTakePhotoEnabled;
    }
  }
}
