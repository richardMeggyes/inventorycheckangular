import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDataColumnsComponent } from './add-data-columns.component';

describe('AddDataColumnsComponent', () => {
  let component: AddDataColumnsComponent;
  let fixture: ComponentFixture<AddDataColumnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDataColumnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDataColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
