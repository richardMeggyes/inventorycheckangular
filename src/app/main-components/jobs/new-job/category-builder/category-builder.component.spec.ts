import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryBuilderComponent } from './category-builder.component';

describe('CategoryBuilderComponent', () => {
  let component: CategoryBuilderComponent;
  let fixture: ComponentFixture<CategoryBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
