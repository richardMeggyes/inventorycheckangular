import {Component, OnInit} from '@angular/core';
import {CategoryModel} from '../../../../shared/models/categoryModel';
import {SubCategory} from '../../../../shared/models/SubCategory';

@Component({
  selector: 'app-category-builder',
  templateUrl: './category-builder.component.html',
  styleUrls: ['./category-builder.component.css']
})
export class CategoryBuilderComponent implements OnInit {

  categoryFieldName: string;
  categoriesArray: Array<CategoryModel>;

  constructor() {
  }

  ngOnInit() {
    this.categoriesArray = new Array<CategoryModel>();

    this.categoriesArray.push(new CategoryModel('Category 1',
      new Array<SubCategory>(new SubCategory('Subcategory 11'), new SubCategory('Category 1'))));
    this.categoriesArray.push(new CategoryModel('Category 2',
      new Array<SubCategory>(new SubCategory('Subcategory 21'), new SubCategory('Subcategory 22'))));
    this.categoriesArray.push(new CategoryModel('Category 3',
      new Array<SubCategory>(new SubCategory('Category 2'), new SubCategory('Subcategory 32'))));
    this.categoriesArray.push(new CategoryModel('Category 4',
      new Array<SubCategory>(new SubCategory('Subcategory 41'), new SubCategory('Subcategory 42'))));

    this.checkCategoryHighlight();
  }

  getMaxSubCategoriesArray() {
    let maxNumberOfSubcategories = 0;
    for (const categoryModel of this.categoriesArray) {
      if (categoryModel.subcategories.length > maxNumberOfSubcategories) {
        maxNumberOfSubcategories = categoryModel.subcategories.length;
      }
    }
    return Array(maxNumberOfSubcategories).fill(0).map((x, i) => i);
  }

  subCategoryHasName(categoryName) {
    let nameExistInSubCategories = false;

    for (const categoryModel of this.categoriesArray) {
      for (const subCategoryname of categoryModel.subcategories) {
        if (subCategoryname === categoryName) {
          nameExistInSubCategories = true;
        }
      }
    }
    return nameExistInSubCategories;
  }

  categoryHasName(categoryName) {
    let nameExistInCategories = false;

    for (const categoryModel of this.categoriesArray) {
      if (categoryName === categoryModel.name) {
        nameExistInCategories = true;
      }
    }
    return nameExistInCategories;
  }

  addCategoryColumn() {
    this.categoriesArray.push(new CategoryModel('New column', new Array<SubCategory>(new SubCategory('New subcategory'))));
  }

  checkCategoryHighlight() {
    this.resetAllCategoryHighlight();
    for (const categoryModel of this.categoriesArray) {
      for (const categoryModel2 of this.categoriesArray) {
        for (const subCategory of categoryModel2.subcategories) {
          if (subCategory.name === categoryModel.name) {
            if (categoryModel === categoryModel2) {
              categoryModel.error = true;
              subCategory.error = true;
            } else {
              categoryModel.highlight = true;
              subCategory.highlight = true;
            }
          }
        }
      }
    }
  }

  resetAllCategoryHighlight() {
    for (const categoryModel of this.categoriesArray) {
      for (const subCategory of categoryModel.subcategories) {
        if (subCategory.highlight) {
          console.log(subCategory.name + ' ' + subCategory.highlight);
        }
        categoryModel.highlight = false;
        subCategory.highlight = false;
        categoryModel.error = false;
        subCategory.error = false;
      }
    }
  }

  addNewSubCategoryToCategoryModel(categoryModel: CategoryModel) {
    categoryModel.subcategories.push(new SubCategory('New subcategory'));
  }

  checkCategoryCicleError() {
    for (const categoryModel of this.categoriesArray) {
      for (const subcategory of categoryModel.subcategories) {
        if (subcategory.name === categoryModel.name) {
          categoryModel.error = true;
          subcategory.error = true;
        } else {
          categoryModel.error = false;
          subcategory.error = false;
        }
      }
    }
  }
}
