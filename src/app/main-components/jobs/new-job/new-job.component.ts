import {Component, OnInit, Output, ComponentFactoryResolver, ViewChild, ViewContainerRef, ComponentRef, Input} from '@angular/core';

import {FileUploadService} from '../../../shared/services/file-upload.service';
import {Tablefile} from '../../../shared/models/tablefile';
import {TableColumn} from '../../../shared/models/table-columns/table-column';
import {CategoryBuilderComponent} from './category-builder/category-builder.component';
import {ColumnTypeProvider} from '../../../providers/ColumnTypes';
import {CategoryColumn} from '../../../shared/models/table-columns/category-column';
import {SelectorColumn} from '../../../shared/models/table-columns/selector-column';
import {ExamplePlaceHolderProvider} from '../../../providers/ExamplePlaceHolders';
import {TextColumn} from '../../../shared/models/table-columns/text-column';
import {PhotoColumn} from '../../../shared/models/table-columns/photo-column';
import {DataTypeProvider} from '../../../providers/DataType';
import {ServerService} from '../../../services/server.service';
import {NotificationService} from '../../../shared/services/notification.service';

@Component({
  selector: 'app-new-job',
  templateUrl: './new-job.component.html',
  styleUrls: ['./new-job.component.css']
})
export class NewJobComponent implements OnInit {

  jobId: string;

  @Input('jobId') set jobIdChange(value) {
    this.jobId = value;
  }

  // Tab related variables
  verificationTab = false;
  exploratoryTab = false;

  fileDataReference = 'reference';
  fileDataCostCenter = 'costCenter';

  // Files
  referenceTableFile = new Tablefile();
  ccTableFile = new Tablefile();

  // Table columns
  @Output() tableColumns: Array<any>;

  // Dynamic component loading
  componentClass = CategoryBuilderComponent;
  components = new Array<ComponentRef<CategoryBuilderComponent>>();
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  // Provider
  columnType: ColumnTypeProvider;
  examplePlaceholder: ExamplePlaceHolderProvider;
  dataTypes: DataTypeProvider;

  constructor(private fileUploadService: FileUploadService,
              private componentFactoryResolver: ComponentFactoryResolver,
              columnType: ColumnTypeProvider,
              examplePlaceholder: ExamplePlaceHolderProvider,
              dataTypes: DataTypeProvider,
              private serverService: ServerService,
              private notificationService: NotificationService) {
    this.columnType = columnType;
    this.examplePlaceholder = examplePlaceholder;
    this.dataTypes = dataTypes;
  }

  ngOnInit() {
    const foundColumn = new TableColumn('Found', this.columnType.SWITCH);
    const numberColumn = new TableColumn('Number', this.columnType.NUMBER);
    const selectorColumn = new SelectorColumn('Condition', this.columnType.SELECTOR, this.examplePlaceholder.SELECTOR_COLUMN_EXAMPLE);
    const textColumn = new TextColumn('Location', this.columnType.TEXT);
    const CCColumn = new TableColumn('Cost Center', this.columnType.CCID);
    const photoColumn = new PhotoColumn('Photo of the object', this.columnType.PHOTO);
    const categoryColumn = new CategoryColumn('Category selector', this.columnType.CATEGORY_SELECTOR, 'CATEGORY_1');
    this.addNewCategoryBuilderComponent('CATEGORY_1');

    this.tableColumns = Array<TableColumn>(
      foundColumn, numberColumn, selectorColumn, textColumn, CCColumn, photoColumn, categoryColumn);

    this.verificationTab = true;
  }

  tabSelect(tabSelect) {
    if (tabSelect === 'inprogress') {
      this.verificationTab = true;
      this.exploratoryTab = false;
    } else if (tabSelect === 'done') {
      this.verificationTab = false;
      this.exploratoryTab = true;
    }
  }

  categoryColumnAdded(event) {
    this.addNewCategoryBuilderComponent(event);
  }

  addNewCategoryBuilderComponent(categoryFieldName: string) {
    // Create component dynamically inside the ng-template
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.componentClass);
    const componentRef = this.container.createComponent(componentFactory);
    componentRef.instance.categoryFieldName = categoryFieldName;

    // Push the component so that we can keep track of which components are created
    this.components.push(componentRef);
  }

  removeCategoryBuilderComponent(categoryId) {
    console.log('remove catbuilder ' + categoryId);
    let componentToRemove: number;

    for (const categoryComponent of this.components) {
      if (categoryComponent.instance.categoryFieldName === categoryId) {
        componentToRemove = this.components.indexOf(categoryComponent);
        break;
      }
    }
    this.container.remove(componentToRemove);
    this.components.splice(componentToRemove, 1);
  }

  buildJSONJobConfiguration() {
    const jobJSONConfiguration = {};
    if (this.jobId === undefined) {
      jobJSONConfiguration['jobId'] = 'undefined';
    } else {
      jobJSONConfiguration['jobId'] = this.jobId;
    }
    if (this.referenceTableFile.header !== undefined) {
      jobJSONConfiguration[this.dataTypes.REFERENCE] = this.referenceTableFile;
    }
    if (this.ccTableFile.header !== undefined) {
      jobJSONConfiguration[this.dataTypes.COST_CENTER] = this.ccTableFile;
    }
    jobJSONConfiguration['workingTable'] = this.tableColumns;

    for (const categoryComponent of this.components) {
      jobJSONConfiguration[categoryComponent.instance.categoryFieldName] = categoryComponent.instance.categoriesArray;
    }
    return <JSON>jobJSONConfiguration;
  }

  onCreateNewJob() {
    this.serverService.createNewJob(this.buildJSONJobConfiguration());
  }
}
