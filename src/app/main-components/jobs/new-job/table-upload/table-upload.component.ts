import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Tablefile} from '../../../../shared/models/tablefile';
import {DataTypeProvider} from '../../../../providers/DataType';
import {ServerService} from '../../../../services/server.service';

@Component({
  selector: 'app-table-upload',
  templateUrl: './table-upload.component.html',
  styleUrls: ['./table-upload.component.css']
})
export class TableUploadComponent implements OnInit {

  // File upload variables
  public files: Set<File> = new Set();
  selectedFile: File;

  // Csv data variables
  csvDataReceived = false;
  csvDataHeadersSave: Array<string>;
  csvData: any;
  selectedCostCenterColumnName = '';

  // csvData test
  csvDataHeadersTestR = ['Koltseghely', 'aruhaz neve', 'aruhaz tipusa', 'Eszkoz szama',
    'Statusz', 'Elhelyezkedes', 'Kategoria', 'Alkategoria', 'Eszkoz tipus', 'Eszk?z Neve',
    'Gyartasi sorozatszam', 'Medellszam', 'Gyarto'];
  csvDataTestR = ['1068,Dabas  - 41022,Hypermarket,3000005462,Mukodik,rakt?r,Fixtures and Fittings,Refrigeration Medium Life,Coldroom Evaporator,hutokamra elparologtato,400/3232379,S-GDF 031B/24,Nem ismert',
    '01068,Dabas  - 41022,Hypermarket,3000005463,Mukodik,rakod? udvar,Fixtures and Fittings,Mechanical Handling Equipment,Non Powered Hand Pallet Truck,Kezi raklapemelo (beka),,,Nem ismert',
    '01068,Dabas  - 41022,Hypermarket,3000005464,Mukodik,m?szaki helyis?g,Fixtures and Fittings,Electrical Equipment,Main Switchboard,Foeloszto,,,Nem ismert',
    '01068,Dabas  - 41022,Hypermarket,3000005465,Mukodik,Kaz?n,Fixtures and Fittings,HVAC Short Life,Water Softener,Vizlagyito,,Vad-15-f1 eco,Nem ismert',
    '01068,Dabas  - 41022,Hypermarket,3000005466,Mukodik,Kaz?n,Fixtures and Fittings,HVAC Short Life,HWS Circulation Pump,Futesi szivattyu,2039651,Top e50,Wilo'];

  csvDataHeadersTestCC = ['Store name', 'Store size', 'Store number', 'Technician e-mail address', 'Technician name', 'Phone number'];
  csvDataTestCC = ['Ajka,5K,41770,41770kar@hu.tesco-europe.com; ,Erd?lyi Imre,20/827-4636',
    'Ar?na Pl?za,7K,41014,41014kar@hu.tesco-europe.com; ,Schneider P?ter,20/827-9888',
    'Baja,7K,41650,41650kar@hu.tesco-europe.com; ,Szalai Istv?n,20/827-1757',
    'Balassagyarmat 3k,3K,41019,41019kar@hu.tesco-europe.com; ,Sz?plaki Gyula,20/821-0383',
    'Balassagyarmat 3k,3K,41019,41019kar@hu.tesco-europe.com; ,Liki G?bor,20/827-0384'];

  @Input() dataType: string;

  @Input() tableFile: Tablefile;

  jobId: string;
  @Output() jobIdChange: EventEmitter<string> = new EventEmitter<string>();

  // @Input('jobId') set jobIdChange(value) {
  //   this.jobId = value;
  // }

  // Providers
  dataTypes: DataTypeProvider;

  constructor(private http: HttpClient, dataTypes: DataTypeProvider, private serverService: ServerService) {
    this.dataTypes = dataTypes;
  }

  ngOnInit() {
    this.tableFile.dataType = this.dataType;
  }

  testCC() {
    this.csvData = this.csvDataTestCC;
    this.csvDataReceived = true;

    this.tableFile.header = this.csvDataHeadersTestCC;
    this.tableFile.dataType = this.dataType;
    this.tableFile.localFileName = 'local file name';
    this.tableFile.fileId = 999;
  }

  testR() {
    this.tableFile.header = this.csvDataHeadersTestR;
    this.csvData = this.csvDataTestR;
    this.csvDataReceived = true;

    this.tableFile.dataType = this.dataType;
    this.tableFile.localFileName = 'local file name';
    this.tableFile.fileId = 999;
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    const fileName = file.name;
    if (fileName.substr(fileName.length - 4, 4) === '.csv') {
      console.log('Valid csv file selected, uploading...');
      this.selectedFile = file;
      this.onUpload();
    } else {
      console.log('Not a CSV file');
      // this.notifier.notify( 'success', 'You are awesome! I mean it!' );
    }
  }

  onUpload() {

    // Adding CORS header
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': '*',
        'Access-Control-Allow-Methods': '*'
      })
    };

    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    uploadData.append('dataType', this.tableFile.dataType);

    if (!this.jobId === undefined) {
      uploadData.append('jobId', this.jobId.toString());
    } else {
      uploadData.append('jobId', 'undefined');
    }
    this.tableFile.localFileName = this.selectedFile.name;
    this.http.post(this.serverService.serveraddress + 'upload', uploadData, httpOptions)
      .subscribe(event => {
        console.log('Upload done');
        this.handleResponseCsvData(event);
      });
  }

  handleResponseCsvData(event) {
    console.log('Parsing event');
    this.tableFile.header = event.header.split(',');
    this.csvData = event.data;
    this.csvDataReceived = true;
    this.tableFile.dataType = this.dataType;
    this.tableFile.localFileName = this.selectedFile.name;
    this.tableFile.fileId = event.fileId;
  }

  test() {
    console.log(this.tableFile.toString());
  }

  switchCsvHeader() {
    if (!this.tableFile.firstRowIsData) {
      this.csvDataHeadersSave = this.tableFile.header;
      console.log(this.tableFile.header);
      const rowsNum = this.tableFile.header.length + 1;
      this.tableFile.header = [];
      for (let _i = 0; _i < rowsNum; _i++) {
        const asd = 'COL_' + _i.toString();
        this.tableFile.header.push(asd);
      }
      this.tableFile.header = this.tableFile.header.slice(0, -1); // removing colon from the end
    } else {
      this.tableFile.header = this.csvDataHeadersSave;
    }
    this.tableFile.firstRowIsData = !this.tableFile.firstRowIsData;
  }

  setCostCenterColumnInReference(id) {
    this.tableFile.ccColumn = id;

    let counter = 0;
    for (const header of this.tableFile.header) {
      if (counter === id) {
        this.selectedCostCenterColumnName = header;
        break;
      }
      counter++;
    }
  }

  indexTracker(index: number, value: any) {
    return index;
  }

  indexTracker2(index: number, value: any) {
    return index;
  }

}
