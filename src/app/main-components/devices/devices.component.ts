import {Component, OnInit} from '@angular/core';
import {ServerService} from '../../services/server.service';
import {NotificationService} from '../../shared/services/notification.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {

  devices: JSON[];

  constructor(private serverService: ServerService,
              private notificationService: NotificationService) {
  }

  ngOnInit() {
    this.loadDevices();
  }

  testNewDevice() {
    const currentDevices = this.devices.length;
    const success = this.serverService.addnewDevice('name', 'user', 'deviceholdersname');
    this.reloadDevices(success, currentDevices);
  }

  reloadDevices(success, currentDevices, reloadCounter = 0) {
    if (reloadCounter < 10) {
      if (success) {
        setTimeout(() => this.loadDevices(), 100);
        setTimeout(() => {
          if (currentDevices === this.devices.length) {
            this.reloadDevices(success, currentDevices, reloadCounter + 1);
          }
        }, 1000);
      }
    } else {
      this.notificationService.showError('Please refresh the page.', 'Failed to reload devices!');
    }
  }

  loadDevices() {
    this.serverService.getDevices().subscribe(event => {
        if (event['status'] === 'success') {
          this.devices = <JSON[]>JSON.parse(event['devices']);
        } else if (event['status'].contains('Error:')) {
          this.notificationService.showError('Failed to load devices', event['status']);
        }
      },
      error => this.serverService.handleHttpError(error));
  }
}
