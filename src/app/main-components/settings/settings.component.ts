import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {ServerService} from '../../services/server.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  isAuthenticated: boolean;

  token: TokenStorageService;

  // CORS header
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    })
  };

  constructor(private tokenService: TokenStorageService, private serverService: ServerService, private http: HttpClient) {
    this.token = tokenService;
  }

  ngOnInit() {
    this.isAuthenticated = this.token.isAuthenticated();
  }

  signup() {

  }

  deleteUser() {
    this.serverService.deleteUser().subscribe(data => {
      console.log(data);
      this.tokenService.signOut();
      window.location.reload();
    });
  }

  handleHttpError(error) {
    console.log(error);
  }

  test_shit() {
    const uploadData = new FormData();
    uploadData.append('arveres_id', '531468');
    uploadData.append('arverestetel_id', '369624');
    return this.http.post('https://arveres.mbvk.hu/reszletezo.php', uploadData, this.httpOptions)
      .subscribe(event => {
          console.log(event.toString());
        },
        error => this.handleHttpError(error));
  }
}
