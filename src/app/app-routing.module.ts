import {NgModule} from '@angular/core';
import {RouterModule, Routes, CanActivate} from '@angular/router';
import {MainLayoutComponent} from './layouts/main-layout/main-layout.component';
import {JobsComponent} from './main-components/jobs/jobs.component';
import {NewJobComponent} from './main-components/jobs/new-job/new-job.component';
import {DevicesComponent} from './main-components/devices/devices.component';
import {SettingsComponent} from './main-components/settings/settings.component';
import {DashboardComponent} from './main-components/dashboard/dashboard.component';
import {RegisterComponent} from './auth/register/register.component';
import {LoginComponent} from './auth/login/login.component';
import {AdminComponent} from './main-components/admin/admin.component';
import {UserComponent} from './user/user.component';
import {HomeComponent} from './main-components/home/home.component';
import {NopermissionComponent} from './auth/nopermission/nopermission.component';
import {ConvertLogComponent} from './main-components/convert-log/convert-log.component';
import {RoleGuardService} from './auth/role-guard.service';
import {DebugToolsComponent} from './main-components/debug-tools/debug-tools.component';

const routes: Routes = [
  {path: 'nopermission', component: NopermissionComponent},
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'jobs',
    component: MainLayoutComponent,
    children: [
      {path: '', component: JobsComponent},
      {path: 'new', component: NewJobComponent},
    ],
    // canActivate: [AuthGuard]
  },
  {
    path: 'devices',
    component: MainLayoutComponent,
    children: [
      {
        path: '', component: DevicesComponent,
        // canActivate: [RoleGuard],
        // data: {
        //   expectedRole: 'ROLE_ADMIN'
        // },
      }
    ],
  },
  {
    path: 'settings',
    component: MainLayoutComponent,
    children: [
      {path: '', component: SettingsComponent}
    ],
    // canActivate: [AuthGuard]
  },
  {
    path: 'dashboard',
    component: MainLayoutComponent,
    children: [
      {path: '', component: DashboardComponent}
    ],
    // canActivate: [AuthGuard]
  },

  {
    path: 'convert-log',
    component: MainLayoutComponent,
    children: [
      {path: '', component: ConvertLogComponent}
    ],
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'ROLE_CONVERT'
    },
  },

//  AUTH
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'user',
    component: UserComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },

  // Debug
  {
    path: 'debug',
    component: MainLayoutComponent,
    children: [
      {path: '', component: DebugToolsComponent}
    ],
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'ROLE_DEBUG'
    },
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes)],
})

export class AppRoutingModule {
}
