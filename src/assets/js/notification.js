function showNotificationShort(type, from, align, msg){
  showNotification(type, from, align, msg, 3000)
}

export function showNotificationLong(type, from, align, msg){
  showNotification(type, from, align, msg, 5000)
}

function showNotification(type, from, align, msg, length) {
  // type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];
  let icon = 'notifications';
  if (type === 'danger'){
    icon = 'error';
  } else if (type === 'warning'){
    icon = 'warning';
  } else if (type === 'success'){
    icon = 'check';
  } else if (type === 'info'){
    icon = 'info';
  } else if (type === 'rose'){
    icon = 'insert_emoticon';
  } else if (type === 'primary'){
    icon = 'notifications';
  }

  $.notify({
    icon: icon,
    message: msg

  }, {
    type: type,
    timer: length,
    placement: {
      from: from,
      align: align
    }
  });
}

function testNotifications(){
  showNotificationLong('info', 'top', 'right', 'This is a Info notificaiton');
  showNotificationLong('primary', 'top', 'right', 'This is a Primary notificaiton');
  showNotificationLong('success', 'top', 'right', 'This is a Success notificaiton');
  showNotificationLong('warning', 'top', 'right', 'This is a Warning notificaiton');
  showNotificationLong('danger', 'top', 'right', 'This is a Danger notificaiton');
  showNotificationLong('rose', 'top', 'right', 'This is a Rose notificaiton');
}
